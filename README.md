# EdgeOS script to install Google Authenticator PAM modules on a UBNT device.

If the Google Authenticator PAM modules aren't installed on the device, this script will
download the latest version and install it. This script also handles installing the
Vyatta Google Authentication configuration and operation package. All configuration for
Google Authenticator is made via the Vyatta Google Authenticator commands.

# Installation

If you prefer to ensure the Google Authenticator framework is retrained across device
upgrades, copy the script into the /config/scripts/post-config.d/ directory. Once copied,
either execute the script with elevated privileges, or wait until the device is rebooted.

# Dependencies

The following projects are installed by this script.

* [Google Authenticator PAM module](https://github.com/google/google-authenticator-libpam)
* [Vyatta Google Authenticator](https://bitbucket.org/mdailous/vyatta-google-authenticator)
