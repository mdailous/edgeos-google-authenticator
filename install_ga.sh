#!/bin/bash

#######################################################
#
# Installs the Google Authenticator PAM framework and (optionally) the Vyatta Google
# Authenticator configuration and operation package onto an EdgeOS device.
#
# Copyright (c) 2016-2017 Michael Dailous.
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE 
# OR OTHER DEALINGS IN THE SOFTWARE.
#
########################################################

source /opt/vyatta/etc/functions/script-template

# Global variable declaration
SCRIPT=${0##*/}
echo "Script name: $SCRIPT"
LOG_FILE="/var/log/post-config.d/${SCRIPT%\.*}.log"
echo "Log file: $LOG_FILE"
DL_DIR='/tmp/'
echo "Download directory: $DL_DIR"

GA_LIB_REPO='ftp://ftp.debian.org/debian/pool/main/g/google-authenticator/'
VYGA_LIB_REPO='ftp://ftp.drivehq.com/mdailous/PublicFolder/'

# addLog
# Writes the date/time and passed in argument(s) to the log file.
addLog() {
	printf "%s $@\n" "$(date)" >> $LOG_FILE
}

# addRepository
# Adds the debian wheezy repository, if necessary
#
# PARAMETERS:
# NONE
addRepository() {
	if [ -z "(grep '^deb .* # wheezy #$' /etc/apt/sources.list)" ]; then
		addLog "Adding Debian repository for package dependencies"
		configure
		set system package repository wheezy components 'main contrib non-free'
		set system package repository wheezy distribution wheezy
		set system package repository wheezy url http://http.us.debian.org/debian
		commit
		save
		exit
		addLog "Debian repository added to system configuration."
	fi
	apt-get update
}

# packageInstalled
# Checks if the specified package is already installed
#
# PARAMETERS:
# $1: Name of package to check.
#
# RETURNS:
# 0: Package is already installed.
# 1: Package is not installed.
packageInstalled() {
	local __lib="$1"
	addLog "Checking if package [$__lib] is installed"
	if [ ! -z "$(dpkg --get-selections $__lib 2>/dev/null)" ]; then
		return 0
	else
		return 1
	fi
}

# getLatestPkgName
# Determines the latest version of the specified library available for download.
# 
# PARAMETERS:
# $1: The library name
# $2: The architecture
# $3: The URL to download from
# dpkg -s libpam-google-authenticator
# dpkg --info /tmp/lib*.deb
getLatestPkgName() {
	local __lib="$1"
	local __arch="${2,,}"
	local __repo="${3%*/}"
	addLog "Determining most recent library [$__lib] for architecture [$__arch] in repo [$__repo]"
	echo $(curl -# -S $__repo 2>/dev/null | grep "$__lib_.*_$__arch.deb" | awk '{print $NF}' | tail -1)
}

# performInstall
#
# PARAMETERS:
# $1: Full path to installable package
#
# RETURNS:
# 0: Package was successfully installed
# 1: Package failed to install
performInstall() {
	local pkgName="${1##*/}"
	addLog "Attempting to install package [$pkgName]"
	dpkg -i $1
	if [ $? == 0 ]; then
		return 0
	else
		# dpkg failed. Most likely reason is due to dependencies.
		# Try to resolve the dependencies and re-attempt install.
		addLog "Installing package dependencies"
		apt-get -f -y install
		return $?
	fi
}


# installPackage
# Attempts to install the specified package.
#
# PARAMETERS:
# $1: Library name
# $2: Download repository location. [NOTE: If this value matches parameter the directory
#     to download the file, no online lookup is performed. Instead, the function installs
#     the file located in the download directory.]
# $3: Library architecture needed
# $4: Directory to download file
#
# RETURNS:
# 0: success: The package is either already installed, or has bee installed
#             successfully.
# 1: failure. The package is not installed and the installation failed.
installPackage() {
	local __lib="$1"
	local __repo="$2"
	local __arch="$3"
	local __base="${4%*/}"
	echo "Download files location: $__base"

	if packageInstalled $__lib; then
		addLog "Package [$__lib] has already been installed"
		return 0
	fi

	addLog "[$__lib] is not installed. Determining most recent library for architecture [$__arch] in repo [$__repo]"
	
	local deb_pkg=
	# TODO: Figure out how to tell if the repo is a URL or not
	if [ "${__repo%*/}" == "$__base" ]; then
		deb_pkg=$(ls -tr "${__base}/${__lib}"_*_"${__arch}".deb | tail -1)
		deb_pkg="${deb_pkg##*/}"
	else
		deb_pkg=$(curl -# -S $__repo 2>/dev/null | grep "${__lib}_.*_${__arch}.deb" | awk '{print $NF}' | tail -1)
	fi

	# Make sure we got a package name to download, and the package hasn't already been
	# downloaded previously
	if [ -z "$deb_pkg" ]; then
		addLog "Failed to determine package name to install."
	else
		addLog "Checking if [$deb_pkg] has already been downloaded."
		if [ ! -f "$__base/$deb_pkg" ]; then
			addLog "Downloading $deb_pkg from $__repo."
			local dl_path="${__repo%*/}/$deb_pkg"
			$(curl -# -S -o "$__base/$deb_pkg" "$dl_path" 2>/dev/null)
			if [ ! $? == 0 ]; then
				addLog "FAILED download from $__repo."
				return 1
			fi
		fi

		performInstall "$__base/$deb_pkg"
		if [ $? == 0 ]; then
			addLog "$__lib has been successfully installed."
			return 0
		else
			addLog "Installation failed with error code [$?]."
		fi
	fi
	return 1
}

# enableAuthenticator
# Enables the Google Authenticator PAM module upon successful installation. This method
# also ensures the Google AUthenticator PAM module is enabled upon firmware upgrade and
# software automated re-installation.
#
# PARAMETERS:
# NA
#
# RETURNS:
# NA
enableAuthenticator() {
	if [ -z "$(grep 'pam_google_authenticator.so' /etc/pam.d/sshd)" ]; then
		addLog "Enabling the Google Authenticator PAM module"
		configure
		set service ssh google-authenticator
		commit
		save
		exit
		addLog "Finished."
	fi
}

# make sure the log file directory exists. if not, create it.
if [ ! -d "${LOG_FILE%/*}" ]; then
	mkdir -p ${LOG_FILE%/*}
fi


addRepository
installPackage 'libpam-google-authenticator' $GA_LIB_REPO 'mips' $DL_DIR && installPackage 'vyatta-google-authenticator' $VYGA_LIB_REPO 'all' $DL_DIR && enableAuthenticator
